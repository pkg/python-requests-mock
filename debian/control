Source: python-requests-mock
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Corey Bryant <corey.bryant@canonical.com>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools (>= 99~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-fixtures,
 python3-mock,
 python3-purl,
 python3-requests,
 python3-six,
 python3-subunit,
 python3-testtools,
 python3-urllib3,
 subunit,
 testrepository,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-requests-mock
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-requests-mock.git
Homepage: https://github.com/jamielennox/requests-mock
Testsuite: autopkgtest-pkg-python

Package: python-requests-mock-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: mock out responses from the requests package - doc
 requests-mock provides a building block to stub out the HTTP requests_
 portions of your testing code. Everything in requests_ eventually goes through
 an adapter to do the transport work. requests-mock creates a custom adapter
 that allows you to predefine responses when certain URIs are called. There are
 then a number of methods provided to get the adapter used.
 .
  This package contains the documentation.

Package: python3-requests-mock
Architecture: all
Depends:
 python3-requests,
 python3-six,
 ${misc:Depends},
 ${python3:Depends},
Description: mock out responses from the requests package - Python 3.x
 requests-mock provides a building block to stub out the HTTP requests_
 portions of your testing code. Everything in requests_ eventually goes through
 an adapter to do the transport work. requests-mock creates a custom adapter
 that allows you to predefine responses when certain URIs are called. There are
 then a number of methods provided to get the adapter used.
 .
 This package contains the Python 3.x module.
